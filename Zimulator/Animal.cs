﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class Animal : Life
    {

        // food
        protected int foodEnough;           // how much the animal needs to eat each time to get satisfied => hunger == 0
        protected int starvationPoint;      // how many hunger-units animal can survive
        protected int hunger;               // increases by one for each time-unit -- cannot have children if hungry
        protected string foodType;          // what species the animal eats
        
        // position
        protected int speed;                // number of squares it can move in one time-unit


        public Animal(Pos p, int ageIn, uint idIn, ref WorldArea worldAreaIn, ref DynamicValues ValueClassIn)
            : base(p, ageIn, idIn, ref worldAreaIn, ref ValueClassIn)
        {

            speed = ValueClass.getSpeed(race);
            foodEnough = ValueClass.getFoodEnough(race);
            starvationPoint = ValueClass.getStarvationPoint(race);

        }


        // *** age
        // Life.increaseAge()


        // *** food
        // TODO try to generalize food-type specified on who is eating - this means finding out what object is eating first
        internal override void feed()
        {
            hunger++;

            if (hunger > starvationPoint)
            { state = State.DEAD; }

            if (state != State.DEAD)
            {
                TryToEat();
            }
        }

        internal void TryToEat()
        {




            // urk!
            // GÖr snyggare!






            Area[,] thisArea = worldArea.getAreaArray();
            int foodItemCounter = 0;

            foreach (Life l in thisArea[here.getX(), here.getY()].getObjectsInArea())
            {
                // check string foodtype to see if the variable race matches
                if (this.foodType.Equals(l.GetRace(), StringComparison.OrdinalIgnoreCase))
                {
                    // System.Windows.Forms.MessageBox.Show("i animal: Nu äter djuret.");
                    if (l.isAlive())
                    {
                        l.logger("Got eaten by a " + this.GetRace());
                        l.gotAssKicked();
                        foodItemCounter++;
                        this.logger("äta\n");
                        hunger = 0;
                        if (foodItemCounter == foodEnough)
                        { break; }
                    }
                }
            }
        }

        // *** children
        internal override List<Life> reproduce()
        {
            List<Life> childObjects = new List<Life>();
            if (state == State.MATURE)
            {
                // makeBabies(ref childObjects);
            }
            return childObjects;
        }


        // *** position
        // Life.getPosX()
        // Life.getPosY()

        internal override void move(bool lazy)
        {
            this.logger("Jag är i x: " + Convert.ToString(here.getX()) + ", y:" + Convert.ToString(here.getY()));
            NewRandomClosePos nrcp = new NewRandomClosePos();
            Pos tempPos = nrcp.getNewPos(here, speed, lazy, ref ValueClass);

            if(worldArea.areaArray[tempPos.getX(), tempPos.getY()].canThisBeAdded(this))
            {

                this.worldArea.moveFrom(this);
                // System.Windows.Forms.MessageBox.Show("i Animal: " + Convert.ToString(this.GetObjectId() + "is the object id and the position is: " + Convert.ToString(here.getX()) + ", " + Convert.ToString(here.getY())));


                here.setX(tempPos.getX());
                here.setY(tempPos.getY());

                // System.Windows.Forms.MessageBox.Show("i Animal: " + Convert.ToString(this.GetObjectId() + "is the object id and the position is: " + Convert.ToString(here.getX()) + ", " + Convert.ToString(here.getY())));
                this.worldArea.moveTo(this, false);
                this.logger("Jag gick till x: " + Convert.ToString(here.getX()) + ", y:" + Convert.ToString(here.getY()));
            }

        }


        // *** die
        // Life.gotAssKicked()
        // Life.isAlive()

    }
}
