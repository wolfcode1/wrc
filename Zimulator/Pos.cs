﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class Pos
    {
        private int x;
        private int y;

        public Pos(int xIn, int yIn)
        {
            x = xIn;
            y = yIn;
        }

        public int getX()
        { return x; }

        public int getY()
        { return y; }

        public void setX(int inX)
        { x = inX; }

        public void setY(int inY)
        { y = inY; }

        public void addX(int xIn)
        { x = x + xIn; }

        public void addY(int yIn)
        { y = y + yIn; }
    }
}
