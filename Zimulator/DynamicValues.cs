﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class DynamicValues
    {
        private int lifeMaxAge = 0;
        private int wolfMaxAge = 10;
        private int rabbitMaxAge = 10;
        private int carrotMaxAge = 6;

        public bool setMaxAge(string race, int ageIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfMaxAge = ageIn;
                    return true;
                case "rabbit":
                    rabbitMaxAge = ageIn;
                    return true;
                case "carrot":
                    carrotMaxAge = ageIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getMaxAge(string race)
        { 
            switch(race)
            {
                case "wolf":
                    return wolfMaxAge;
                case "rabbit":
                    return rabbitMaxAge;
                case "carrot":
                    return carrotMaxAge;
                case "life":
                    return lifeMaxAge;
                default:
                    return -1;
            }
        }
        
        private int lifeInitialIndividuals = 0;
        private int wolfInitialIndividuals = 50;
        private int rabbitInitialIndividuals = 50;
        private int carrotInitialIndividuals = 100;

        public bool setInitialIndividuals(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfInitialIndividuals = intIn;
                    return true;
                case "rabbit":
                    rabbitInitialIndividuals = intIn;
                    return true;
                case "carrot":
                    carrotInitialIndividuals = intIn;
                    return true;
                default:
                    return false;
            }
        }
        
        public int getInitialIndividuals(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfInitialIndividuals;
                case "rabbit":
                    return rabbitInitialIndividuals;
                case "carrot":
                    return carrotInitialIndividuals;
                case "life":
                    return lifeInitialIndividuals;
            }
            return -1;
        }

        private int wolfFertilityAge = 3;
        private int rabbitFertilityAge = 1;
        private int carrotFertilityAge = 1;
        private int lifeFertilityAge = 0;

        public bool setFertilityAge(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfFertilityAge = intIn;
                    return true;
                case "rabbit":
                    rabbitFertilityAge = intIn;
                    return true;
                case "carrot":
                    carrotFertilityAge = intIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getFertilityAge(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfFertilityAge;
                case "rabbit":
                    return rabbitFertilityAge;
                case "carrot":
                    return carrotFertilityAge;
                case "life":
                    return lifeFertilityAge;
            }
            return -1;
        }

        private int wolfFertilityNumber = 2;
        private int rabbitFertilityNumber = 6;
        private int carrotFertilityNumber = 3;
        private int lifeFertilityNumber = 0;

        public bool setFertilityNumber(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfFertilityNumber = intIn;
                    return true;
                case "rabbit":
                    rabbitFertilityNumber = intIn;
                    return true;
                case "carrot":
                    carrotFertilityNumber = intIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getFertilityNumber(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfFertilityNumber;
                case "rabbit":
                    return rabbitFertilityNumber;
                case "carrot":
                    return carrotFertilityNumber;
                case "Life":
                    System.Windows.Forms.MessageBox.Show("i ValueClass: Nu fick vi fel fråga!");
                    return lifeFertilityNumber;
            }
            return -1;
        }

        // maximum number of food each animal eats at a time
        private int wolfFoodEnough = 2;
        private int rabbitFoodEnough = 2;
        private int lifeFoodEnough = 0;

        public bool setFoodEnough(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfFoodEnough = intIn;
                    return true;
                case "rabbit":
                    rabbitFoodEnough = intIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getFoodEnough(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfFoodEnough;
                case "rabbit":
                    return rabbitFoodEnough;
                case "life":
                    return lifeFoodEnough;
            }
            return -1;
        }

        private int wolfStarvationPoint = 5;
        private int rabbitStarvationPoint = 3;
        private int lifeStarvationPoint = 0;

        public bool setStarvationPoint(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfFoodEnough = intIn;
                    return true;
                case "rabbit":
                    rabbitFoodEnough = intIn;
                    return true;
                case "life":
                    lifeFoodEnough = intIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getStarvationPoint(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfStarvationPoint;
                case "rabbit":
                    return rabbitStarvationPoint;
                case "life":
                    return lifeStarvationPoint;
            }
            return -1;
        }


        private int wolfSpeed = 4;
        private int rabbitSpeed = 2;
        private int lifeSpeed = 0;

        public bool setSpeed(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfSpeed = intIn;
                    return true;
                case "rabbit":
                    rabbitSpeed = intIn;
                    return true;
                case "life":
                    lifeSpeed = intIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getSpeed(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfSpeed;
                case "rabbit":
                    return rabbitSpeed;
                case "life":
                    return lifeSpeed;
            }
            return -1;
        }

        private int wolfMaxPerArea = 2;
        private int rabbitMaxPerArea = 6;
        private int carrotMaxPerArea = 20;

        public bool setLifeMaxPerArea(string race, int intIn)
        {
            switch(race)
            {
                case "wolf":
                    wolfMaxPerArea = intIn;
                    return true;
                case "rabbit":
                    rabbitMaxPerArea = intIn;
                    return true;
                case "carrot":
                    carrotMaxPerArea = intIn;
                    return true;
                default:
                    return false;
            }
        }

        public int getLifeMaxPerArea(string race)
        {
            switch (race)
            {
                case "wolf":
                    return wolfMaxPerArea;
                case "rabbit":
                    return rabbitMaxPerArea;
                case "carrot":
                    return carrotMaxPerArea;
            }
            return -1;
        }

        private int wsx = 25;
        private int wsy = 25;

        public bool setWorldSize(int intInA, int intInB)
        {
            wsx = intInA;
            wsy = intInB;
            return true;
        }

        public Pos getWorldSize() 
        {
            Pos worldSize = new Pos(wsx, wsy);
            return worldSize;
        }

        private int asx = 6;
        private int asy = 6;

        public bool setAreaSize(int intInA, int intInB)
        {
            asx = intInA;
            asy = intInB;
            return true;
        }

        public Pos getAreaSize()
        {
            Pos areaSize = new Pos(asx, asy);
            return areaSize;
        }

        public int getLineThickness()
        {
            int lineThickness = 2;
            return lineThickness;
        }

        private uint objectID = 0;

        public uint getObjectID()
        {
            objectID++;
            return objectID;
        }


        int objectSquareSize = 6;

        public bool setObjectSquareSize(int intIn)
        {
            objectSquareSize = intIn;
            return true;
        }

        public int getObjectSquareSize()
        {
            return objectSquareSize;
        }

        public int getAreaSide()
        {
            // areaside is the size of the area / the size of each square (animal) in the area.
            // return areasize / objectsquaresize
            Pos pos = getAreaSize();
            int side = getObjectSquareSize();
            return (pos.getX() / side);
        }
    }

}
