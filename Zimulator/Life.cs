﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{

    public class Life
    {
        // initial
        protected uint objectId;            // every living object has a unique id
        internal WorldArea worldArea;
        protected string race;              // what type of animal or lifeform is it?
        protected int objectColor;
        protected enum State
        {
            YOUNG,
            MATURE,
            DEAD,
        }
        protected State state;
        protected string logString;
        public DynamicValues ValueClass;

        // age
        protected int age;                  // how old is the life-form?

        // food

        // children
        protected int fertilityAge;

        // position
        protected Pos lastPos;
        protected Pos here;
        protected bool isInAnArea;

        // die
        protected int maxAge;


        public Life(Pos hereIn, int ageIn, uint idIn, ref WorldArea worldAreaIn, ref DynamicValues ValueClassIn)
        {
            ValueClass = ValueClassIn;
            SetRace();
            fertilityAge = ValueClass.getFertilityAge(race);
            maxAge = ValueClass.getMaxAge(race);
            here = hereIn;
            age = ageIn;
            objectId = idIn;
            logString = "Just got born\n";

            worldArea = worldAreaIn;

            checkMaturity();

            isInAnArea = false;

            logger("Object: " + this.GetRace() + " med id: " + Convert.ToString(this.GetObjectId() + "\n"));
        }


        // *** initial
        protected virtual void SetRace()
        { race = "Life"; }

        internal virtual string GetRace()
        { return race; }

        internal virtual uint GetObjectId()
        { return objectId; }

        internal int GetObjectColor()
        { return this.objectColor; }

        public void logger(string inString)
        { logString = logString + inString + "\n"; }

        public string showLog()
        { return logString; }


        // *** age
        internal void increaseAge()
        {
            age++;
            checkMaturity();
            return;
        }

        internal int getAge()
        { return age; }

        internal virtual void checkMaturity()
        {
            switch (state)
            {
                case State.YOUNG:
                    {
                        if (age >= fertilityAge)
                        { state = State.MATURE; }
                    }
                    break;
                case State.MATURE:
                    {
                        if (age >= maxAge)
                        { state = State.DEAD; }
                    }
                    break;
                case State.DEAD:
                    {
                        //Remove();
                    }
                    break;
            }
        }


        // *** food
        internal virtual void feed()
        { return; }


        // *** children
        internal virtual List<Life> reproduce()
        {
            List<Life> a = new List<Life>();
            return a;
        }


        // *** position

        internal int getPosX()
        { return here.getX(); }

        internal int getPosY()
        { return here.getY(); }

        internal virtual void move(bool lazyOrNot)
        { return;  }

        internal void setIsInAnArea()
        { isInAnArea = true; }

        internal bool getIsInAnArea()
        { return isInAnArea; }

        // *** die
        internal void gotAssKicked()
        { state = State.DEAD; }

        internal bool isAlive()
        {
            if (state == State.DEAD)
            { return false; }
            else
            { return true; }
        }





    }
}
