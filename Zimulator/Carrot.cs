﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class Carrot : Plant
    {
        public Carrot(Pos p, int ageIn, uint idIn, ref WorldArea worldAreaIn, ref DynamicValues ValueClassIn)
            : base(p, ageIn, idIn, ref worldAreaIn, ref ValueClassIn)
        {
            this.objectColor = 3;
            //System.Windows.Forms.MessageBox.Show(race + " was now created...");
        }

        // *** initial
        protected override void SetRace()
        { race = "carrot"; }

        // *** age
        // Life.increaseAge()


        // *** food
        // feed()
        // tryToEat()

        // *** children
        internal override List<Life> reproduce()
        {
            List<Life> childObjects = new List<Life>();
            if (state == State.MATURE)
            {
                // check to see if area is already overpopulated - then no new babies...
                if(worldArea.areaArray[here.getX(), here.getY()].getNumberOfObjectsInThisArea() < ValueClass.getLifeMaxPerArea(race))
                {
                    initiateDescendants(ref childObjects);
                }
                
            }
            return childObjects;
        }

        internal void initiateDescendants(ref List<Life> children)
        {
            // a.k.a. makeBabies...
            for (int x = 0; x < ValueClass.getFertilityNumber(race); x++)
            {
                // create a child with a position at most 1 step from the parent
                NewRandomClosePos nrcp = new NewRandomClosePos();

                Carrot newTinyOne = new Carrot(nrcp.getNewPos(here, 5, false, ref ValueClass), 0, ValueClass.getObjectID(), ref worldArea, ref ValueClass);
                newTinyOne.logger("Det blev som vi ville, det blev en " + newTinyOne.GetRace() + " x: " + Convert.ToString(newTinyOne.getPosX()) + " y: " + Convert.ToString(newTinyOne.getPosX()));
                //System.Windows.Forms.MessageBox.Show("in carrot new carrot x = " + newTinyOne.GetPosx() + " och new carrot y = " + newTinyOne.GetPosy());

                children.Add(newTinyOne);
            }
            return;
        }



        // *** position
        // Life.getPosX()
        // Life.getPosY()
        // Life.move()

        // *** die
        // Life.gotAssKicked()
        // Life.isAlive()
    }
}
