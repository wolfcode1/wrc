﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zimulator
{
    public class World
    {
        //Declare an event that carries a "Life" object to all listeners when invoked

        // initialize world
        readonly Pos _worldDimensions;
        List<Life> _worldObjects;
        public WorldArea _worldArea;
        public Task _worker = null;
        public CancellationToken _cancellationToken;
        public CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        public DynamicValues ValueClass;

        public World()
        {
            ValueClass = new DynamicValues();
            _worldDimensions = ValueClass.getWorldSize();
        }

        public string FiatLux()
        {
            _worldObjects = new List<Life>();
            _worldArea = new WorldArea(_worldDimensions, ref ValueClass);

            CreateInitialObjects();

            //_worldArea.AddInitialObjects(_worldObjects);

            return Convert.ToString(_worldObjects.Count());
        }

        public void AddObjectsToAreas()
        {
            _worldArea.AddInitialObjects(_worldObjects);
        }

        public void RunTheWorld()
        {
           //if no worker task (thread) exists, create one and start it.
            if (_worker == null || _worker.Status != TaskStatus.Running)
            {
                _cancellationToken = _cancellationTokenSource.Token;

                _worker = Task.Factory.StartNew(LoopTheWorld, _cancellationToken);
            }
        }

        private void LoopTheWorld()
        {
            while(true)
            {
                Update();
                int carrC = 0;
                int rabbC = 0;
                int wolfC = 0;
                foreach(Life li in _worldObjects)
                {
                    string ra = li.GetRace();
                    switch (ra)
                    {
                        case "wolf":
                            wolfC++;
                            break;
                        case "rabbit":
                            rabbC++;
                            break;
                        case "carrot":
                            carrC++;
                            break;
                    }
                }
                // System.Windows.Forms.MessageBox.Show("i World: Go on, carrots:" + Convert.ToString(carrC) + " rabbits:" + Convert.ToString(rabbC) + " wolves:" + Convert.ToString(wolfC));

                //1. Check that cancellation has not been requested
                if (_cancellationTokenSource.IsCancellationRequested)
                { break; }
            }
        }

        public void StopTheWorld()
        {
            //Request cancellation of task. This could be triggered from a stop button or maybe a pause button.
            _cancellationTokenSource.Cancel();

        }


        public void CreateInitialObjects()
        {
            var carrotStack = new Stack<Life>();
            var rabbitStack = new Stack<Life>();
            var wolfStack = new Stack<Life>();
            // load up all initial objects in their respective stacks
            BuildStacksOfObjects("carrot", ValueClass.getInitialIndividuals("carrot"), ValueClass.getMaxAge("carrot"), ref carrotStack);
            BuildStacksOfObjects("rabbit", ValueClass.getInitialIndividuals("rabbit"), ValueClass.getMaxAge("rabbit"), ref rabbitStack);
            BuildStacksOfObjects("wolf", ValueClass.getInitialIndividuals("wolf"), ValueClass.getMaxAge("wolf"), ref wolfStack);

            int rndPicker = 0;
            while(carrotStack.Count + rabbitStack.Count + wolfStack.Count > 0)
            {
                rndPicker = RandomNumber.Between(1,3);
                switch(rndPicker)
                {
                    case 1:
                        if(carrotStack.Count > 0)
                        { _worldObjects.Add(carrotStack.Pop()); }
                        break;
                    case 2:
                        if(rabbitStack.Count > 0)
                        { _worldObjects.Add(rabbitStack.Pop()); }
                        break;
                    case 3:
                        if(wolfStack.Count > 0)
                        { _worldObjects.Add(wolfStack.Pop()); }
                        break;
                }
            }
        }

        public void BuildStacksOfObjects(string animal, int initialInd, int maxAge, ref Stack<Life> tempStack)
        {
            
            for (int i = 1; i <= initialInd; i++)
            {
                // getting random pos for the animal
                Pos worldMax = ValueClass.getWorldSize();
                int x = RandomNumber.Between(0, (worldMax.getX() - 1));
                int y = RandomNumber.Between(0, (worldMax.getY() - 1));
                Pos tempPos = new Pos(x, y);

                // getting random age for the animal
                int tempAge = 0;
                switch (animal)
                {
                    case "rabbit": 
                        Rabbit tempRabbit = new Rabbit(tempPos, tempAge, ValueClass.getObjectID(), ref _worldArea, ref ValueClass);
                        tempStack.Push(tempRabbit);
                        //System.Windows.Forms.MessageBox.Show("rabbit - age: " + Convert.ToString(tempAge));
                        break;
                    case "wolf":
                        Wolf tempWolf = new Wolf(tempPos, tempAge, ValueClass.getObjectID(), ref _worldArea, ref ValueClass);
                        tempStack.Push(tempWolf);
                        //System.Windows.Forms.MessageBox.Show("wolf - age: " + Convert.ToString(tempAge));
                        break;
                    case "carrot":
                        Carrot tempCarrot = new Carrot(tempPos, tempAge, ValueClass.getObjectID(), ref _worldArea, ref ValueClass);
                        tempStack.Push(tempCarrot);
                        //System.Windows.Forms.MessageBox.Show("carrot - age: " + Convert.ToString(tempAge));
                        break;
                }
            }

            
        }

        public void Update()
        {
            List<uint> allObjects = new List<uint>();
            foreach(Life life in _worldObjects)
            { allObjects.Add(life.GetObjectId()); }

            //System.Windows.Forms.MessageBox.Show("in World: number of objects: " + Convert.ToString(allObjects.Count()));

            foreach(uint number in allObjects)
            {
                List<Life> newChildrenWohoo = new List<Life>();

                foreach(Life l in _worldObjects)
                {
                    if(number == l.GetObjectId())
                    {
                        l.increaseAge();

                        l.feed();

                        newChildrenWohoo.AddRange(l.reproduce());

                        l.move(true);
                    }
                }

                removeDied();

                foreach (Life baby in newChildrenWohoo)
                {
                    baby.logger("i world: någon försöker komma in");
                    if(_worldArea.moveTo(baby, true))
                    { _worldObjects.Add(baby); }
                }

                removeDied();
            }
        }


        public void Update2()
        {
            // order of things updating is this:
            // 1 - age - life must first update its age (and dies if old)
            // 2 - food - life must then eat - if it does
            // 3 - children if not hungry it has children
            // 4 - movement

            // 1 - age
            foreach (Life life in _worldObjects)
            { life.increaseAge(); }

            removeDied();

            // 2 - food
            foreach(Life life in _worldObjects)
            { life.feed(); }
            // some were eaten
            removeDied();

            // 3 - children - reproduce if hungry == 0
            List<Life> newChildrenWohoo = new List<Life>();

            foreach(Life life in _worldObjects)
            { newChildrenWohoo.AddRange(life.reproduce()); }

            foreach (Life baby in newChildrenWohoo)
            {
                _worldArea.moveTo(baby, true);
            }
            
            // 4 - movement
            foreach(Life life in _worldObjects)
            { life.move(true); }

            // System.Windows.Forms.MessageBox.Show("in World: number of items in _worldObjects is now: " + Convert.ToString(_worldObjects.Count()));               

        }

        internal void removeDied()
        {
            for (int i = _worldObjects.Count - 1; i >= 0; i--)
            {
                //System.Windows.Forms.MessageBox.Show("in World: number of items in _worldObjects is now: " + Convert.ToString(_worldObjects.Count()));
                if (_worldObjects[i].isAlive() == false)
                {
                    _worldObjects[i].logger("Age of dying dandy is: " + Convert.ToString(_worldObjects[i].getAge()));
                    //System.Windows.Forms.MessageBox.Show("i world: döende djurets nekrolog: " + _worldObjects[i].showLog());
                    // System.Windows.Forms.MessageBox.Show(Convert.ToString(_worldObjects[i].isAlive()));
                    _worldArea.moveFrom(_worldObjects[i]);
                    // System.Windows.Forms.MessageBox.Show("in world: nu dog " + _worldObjects[i].GetObjectId());
                    _worldObjects[i] = null;
                    _worldObjects.RemoveAt(i);
                }
            }
        }

    }
}
