﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class Rabbit : Animal
    {


        public Rabbit(Pos p, int ageIn, uint idIn, ref WorldArea worldAreaIn, ref DynamicValues ValueClassIn)
            : base(p, ageIn, idIn, ref worldAreaIn, ref ValueClassIn)
        {
            this.objectColor = 2;
            // System.Windows.Forms.MessageBox.Show(race + " was now created...");
            foodType = "carrot";
        }


        // *** initial
        protected override void SetRace()
        { race = "rabbit"; }

        // *** age
        // Life.increaseAge()

        // *** food
        // Animal.feed()
        // Animal.tryToEat()

        // *** children
        internal override List<Life> reproduce()
        {
            List<Life> childObjects = new List<Life>();
            if ((state == State.MATURE) && (hunger == 0))
            {
                if (worldArea.areaArray[here.getX(), here.getY()].getNumberOfObjectsInThisArea() >= ValueClass.getLifeMaxPerArea(this.GetRace()))
                {
                    this.logger("Nuppa\n");
                    createDescendants(ref childObjects);
                }
            }
            return childObjects;
        }

        internal void createDescendants(ref List<Life> children)
        {
            // a.k.a. makeBabies
            for (int x = 0; x < ValueClass.getFertilityNumber(race); x++)
            {
                // create a child with a position at most 1 step from the parent
                NewRandomClosePos nrcp = new NewRandomClosePos();

                Rabbit newTinyOne = new Rabbit(nrcp.getNewPos(here, 1, false, ref ValueClass), 0, ValueClass.getObjectID(), ref worldArea, ref ValueClass);
                newTinyOne.logger("Det blev som vi ville, det blev en " + newTinyOne.GetRace() + " x: " + Convert.ToString(newTinyOne.getPosX()) + " y: " + Convert.ToString(newTinyOne.getPosX()));
                //System.Windows.Forms.MessageBox.Show("in rabbit: new rabbit x = " + newTinyOne.GetPosx() + " och new rabbit y = " + newTinyOne.GetPosy());

                children.Add(newTinyOne);
            }
            return;
        }


        // *** position
        // Life.getPosX()
        // Life.getPosY()
        // Animal.move()


        // *** die
        // Life.gotAssKicked()
        // Life.isAlive()






    }
}
