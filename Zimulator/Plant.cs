﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class Plant : Life
    {

        public Plant(Pos p, int ageIn, uint idIn, ref WorldArea worldAreaIn, ref DynamicValues ValueClassIn) 
            :base(p, ageIn, idIn, ref worldAreaIn, ref ValueClassIn)
        {

        }

        // *** initial
        // *** age
        // Life.increaseAge()

        // *** food
        internal override void feed()
        { return; }


        // *** children
        internal override List<Life> reproduce()
        {
            List<Life> childObjects = new List<Life>();
            if (state == State.MATURE)
            {
                // makeBabies(ref childObjects);
            }
            return childObjects;
        }


        // *** position
        // Life.getPosX()
        // Life.getPosY()
        // Life.move();


        // *** die
        // Life.gotAssKicked()
        // Life.isAlive()
    }
}
