﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class NewRandomClosePos
    {
        private static int x;
        private static int y;
        private static int maxDistance;
        private static int xDir;
        private static int yDir;
        private static int distanceToActuallyMoveOnX;
        private static int distanceToActuallyMoveOnY;
        private static int xMoveOrNot;
        private static int yMoveOrNot;
        public DynamicValues ValueClass;

        public Pos getNewPos(Pos inPos, int maxDistanceIn, bool lazy, ref DynamicValues ValueClassIn)
        {
            ValueClass = ValueClassIn;
            x = inPos.getX();
            y = inPos.getY();
            maxDistance = maxDistanceIn;

            // laziness occurs e.g. when an animal has had enough to eat. When hungry, it is NOT lazy.
            // when lazy it may or may not move, when not lazy it moves for sure (1)
            if(lazy == true)
            {
                // may or may not move, because object is lazy
                xMoveOrNot = RandomNumber.Between(0, 1); // to move or not to move on x
                yMoveOrNot = RandomNumber.Between(0, 1); // to move or not to move on y
                // System.Windows.Forms.MessageBox.Show("in NewRandomClosePos xMoveOrNot = " + xMoveOrNot + " och yMoveOrNOt = " + yMoveOrNot);
            }
            else
            {
                xMoveOrNot = 1;
                yMoveOrNot = 1;
            }

            distanceToActuallyMoveOnX = RandomNumber.Between(maxDistance * -1, maxDistance); // how far to move on x

            distanceToActuallyMoveOnY = RandomNumber.Between(maxDistance * -1, maxDistance); // how far to move on y
            //System.Windows.Forms.MessageBox.Show("in NewRandomClose Posdistance to move x = " + distanceToActuallyMoveOnX + " och distance to move y = " + distanceToActuallyMoveOnY);

            xDir = distanceToActuallyMoveOnX * xMoveOrNot;
            yDir = distanceToActuallyMoveOnY * yMoveOrNot;

            //System.Windows.Forms.MessageBox.Show("in NewRandomClosePos xDir = " + xDir + " och yDir = " + yDir);

            // System.Windows.Forms.MessageBox.Show("in NewRandomClosePos x = " + x + " och y = " + y);
            AddX(xDir, ValueClass.getWorldSize().getX());
            AddY(yDir, ValueClass.getWorldSize().getY());
            //System.Windows.Forms.MessageBox.Show("in NewRandomClosePos x = " + x + " och y = " + y);

            string g = "";

            g = g + "inpos x: " + inPos.getX() + "inpos y: " + inPos.getY() + "\n";
            g = g + "x move or not: " + xMoveOrNot + " y move or not: " + yMoveOrNot + "\n";
            g = g + "xDir: " + xDir + " yDir: " + yDir + "\n";
            g = g + "X: " + x + " y: " + y;
            //System.Windows.Forms.MessageBox.Show(g);



            Pos pos = new Pos(x, y);
            return pos;
        }

        static internal void AddX(int xIn, int worldMaxX)
        {
            x = x + xIn;
            while(x < 0)
            { x++; }

            while(x >= worldMaxX)
            { x--; }

     
        }

        static internal void AddY(int yIn, int worldMaxY)
        {
            y = y + yIn;
            while(y < 0)
            { y++; }
            while(y >= worldMaxY)
            { y--;  }

        }

    }
}
