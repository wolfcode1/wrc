﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Zimulator
{
    public class WorldArea
    {

        // an array of Areas representing the squares where the animals live
        // every Area in the array should have an identity coupled with the coordinates
        // this way we can know to which Area an animal moves when it goes "down" or "left".
        //private Area[,] area;

        private Pos worldSize;
        public Area[,] areaArray;
        public event EventHandler<Zimulator.Area> AreaHandler;
        DynamicValues ValueClass;

        public WorldArea(Pos inSize, ref DynamicValues ValueClassIn)
        {
            ValueClass = ValueClassIn;
            worldSize = inSize;
            areaArray = new Area[worldSize.getX(), worldSize.getY()];
            for(int i = 0; i < worldSize.getX(); i++)
            {
                for (int j = 0; j < worldSize.getY(); j++ )
                {
                    Area newArea = new Area(i, j, ref ValueClass);
                    areaArray[i, j] = newArea;
                }
            }
        }

        internal int AddInitialObjects(List<Life> inObjects)
        {
            foreach(Life l in inObjects)
            {
                if((l != null) && l.isAlive())
                {
                    //System.Windows.Forms.MessageBox.Show(l.GetRace() + "was now added to areaarray");

                    // count to see that this particular type of object is not already overpopulated in this area
                    if(areaArray[l.getPosX(), l.getPosY()].canThisBeAdded(l))
                    {
                        areaArray[l.getPosX(), l.getPosY()].AddObject(l);
                        AreaHandler(this, areaArray[l.getPosX(), l.getPosY()]);
                    }
                    else
                    {
                        l.gotAssKicked();
                    }

                }
            }
            return 0;
        }

        internal void moveFrom(Life lifeIn)
        {
            lifeIn.logger("I worldarea: Nu försöker livet flytta sig från x:" + Convert.ToString(lifeIn.getPosX()) + ", y:" + Convert.ToString(lifeIn.getPosY()));
            if (areaArray[lifeIn.getPosX(), lifeIn.getPosY()].canThisBeDeleted(lifeIn))
            {
                areaArray[lifeIn.getPosX(), lifeIn.getPosY()].RemoveObject(lifeIn);
                AreaHandler(this, areaArray[lifeIn.getPosX(), lifeIn.getPosY()]);
                
            }
            else
            {

                System.Windows.Forms.MessageBox.Show("i WorldArea: Nu är det så uppfuckat så jag kommer på mittmarkeringen på motorvägen - objektet som skulle tas bort fanns inte - fuck!!!");
                System.Windows.Forms.MessageBox.Show(lifeIn.showLog());
                AreaHandler(this, areaArray[lifeIn.getPosX(), lifeIn.getPosY()]);
            }
            
        }



        internal bool moveTo(Life l, bool baby)
        {
            l.logger("I worldarea: Nu försöker livet flytta sig TILL x:" + Convert.ToString(l.getPosX()) + ", y:" + Convert.ToString(l.getPosY()));
            bool truth = false;

            if(baby)
            {
                if (areaArray[l.getPosX(), l.getPosY()].canThisBeAdded(l))
                {
                    areaArray[l.getPosX(), l.getPosY()].AddObject(l);
                    //System.Windows.Forms.MessageBox.Show("i WorldArea: baby comes to world");
                    truth = true;
                }
            }
            else
            {
                areaArray[l.getPosX(), l.getPosY()].AddObject(l);
                //System.Windows.Forms.MessageBox.Show("i WorldArea: x:" + Convert.ToString(l.getPosX()) + " y: " + Convert.ToString(l.getPosY()) + " non-baby moved here");
                truth = true;
            }

            AreaHandler(this, areaArray[l.getPosX(), l.getPosY()]);
            return truth;

        }

        internal void displayPosition (Life lifeIn)
        { AreaHandler(this, areaArray[lifeIn.getPosX(), lifeIn.getPosY()]); }

        internal Area[,] getAreaArray()
        { return areaArray; }



    }



}

 