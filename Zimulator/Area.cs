﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zimulator
{
    public class Area
    {
        // a snapshot of this entire area is to be sent up to the painter function in Form1
        // the size must be made dynamic, but for now it is 5x5 in size
        //public delegate void delegateFunction(object sender, EventArgs e);
        //public event delegateFunction UpdateGrid;
        //public event EventHandler<int[,]> PositionCalculated; 

        // an area is a square
        // it has edges (borders) (x pixels thick)
        // it's width and height must be a multiple of the size of the animals
        // for example if an animal or carrot is 4*4 pixels, the
        // area must be 16*16 or 28*28 pixels INSIDE the borders

        // loop:
        // update the list
        // paint the animals (in the squares)

        // int 0 = tom
        // int 1 = wolf
        // int 2 = rabbit
        // int 3 = carrot

        //public event EventHandler<int[,]> AreaHandler;
        //public event EventHandler<int[,]> AreaHandler;

        private List<Life> objectsInThisArea;
        Pos areaS;
        private uint[,] objectIds;
        public int[,] squareColors;
        public DynamicValues ValueClass;

        private int x;
        private int y;

        public Area(int xIn, int yIn, ref DynamicValues ValueClassIn)
        {
            ValueClass = ValueClassIn;
            areaS = ValueClass.getAreaSize();
            x = xIn;
            y = yIn;
            objectsInThisArea = new List<Life>();
            objectIds = new uint[areaS.getX(), areaS.getY()];
            squareColors = new int[areaS.getX(), areaS.getY()];
        }

        internal int getNumberOfObjectsInThisArea()
        {
            return objectsInThisArea.Count();
        }

        internal bool canThisBeAdded(Life lifeIn)
        {
            bool truth = true;

            int numberOfThisObject = 0;
            foreach(Life li in objectsInThisArea)
            {
                if (li.GetRace().Equals(lifeIn.GetRace()))
                {
                    numberOfThisObject++;
                }
            }
            // is area already overpopulated by this type of object?
            lifeIn.logger("I area: antal likadana object här: " + Convert.ToString(numberOfThisObject));
            lifeIn.logger("I worldarea: antal max är: " + Convert.ToString(ValueClass.getLifeMaxPerArea(lifeIn.GetRace())));
            if (numberOfThisObject >= ValueClass.getLifeMaxPerArea(lifeIn.GetRace()))
            { truth = false;}

            return truth;
        }

        internal bool canThisBeDeleted(Life lifeIn)
        {
            bool truth = false;
            foreach(Life l in objectsInThisArea)
            {
                if(l.GetObjectId() == lifeIn.GetObjectId())
                { truth = true; }
            }
            return truth;
        }

        internal void AddObject(Life lifeIn)
        {
            
            if(recalculateGridAdd(lifeIn) > 0)
            { objectsInThisArea.Add(lifeIn); }
        }


        internal void RemoveObject(Life lifeIn)
        {
            int control = 0;
            int positionOfObject = -1;

            // where does object exist in this area?
            for (int i = objectsInThisArea.Count - 1; i >= 0; i--)
            {
                if(lifeIn.GetObjectId() == objectsInThisArea[i].GetObjectId())
                {
                    positionOfObject = i;
                    control = 1;
                }
            }

            // if it does exist, we remove it, otherwise we need to scream loud
            if(control == 0)
            {
                System.Windows.Forms.MessageBox.Show("i Area: WARNING!! Nu ska objekt " + Convert.ToString(lifeIn.GetObjectId()) + " tas bort från area " + x + ", " + y + "MEN FINNS INTE");
            }
            else
            {
                objectsInThisArea.RemoveAt(positionOfObject);
                recalculateGridRemove(lifeIn);
            }

        }



        //internal int recalculateGridAdd(Life lifeIn)
        //{
        //    // System.Windows.Forms.MessageBox.Show("i Area: Men nu ärejui recalculate...");

        //    int loopControl = 0;
        //    Pos temp = ValueClass.getAreaSize();
        //    for (int x = 0; x < temp.getX(); x++)
        //    {
        //        for (int y = 0; y < temp.getY(); y++)
        //        {
        //            if ((objectIds[x, y] == 0))
        //            {
        //                objectIds[x, y] = lifeIn.GetObjectId();
        //                squareColors[x, y] = lifeIn.GetObjectColor();
        //                lifeIn.setIsInAnArea();

        //                loopControl = 1;
        //            }
        //            if (loopControl == 1)
        //            { break; }
        //        }
        //        if (loopControl == 1)
        //        { break; }
        //    }

        //    return loopControl;
        //}





        internal int recalculateGridAdd(Life lifeIn)
        {
            // System.Windows.Forms.MessageBox.Show("i Area: Men nu ärejui recalculate...");

            int loopControl = 0;
            Pos temp = ValueClass.getAreaSize();
            List<Pos> allSquares = new List<Pos>();
            List<Pos> randSquares = new List<Pos>();

            //System.Windows.Forms.MessageBox.Show("in area: a");

            for (int x = 0; x < temp.getX(); x++)
            {
                for (int y = 0; y < temp.getY(); y++)
                {
                    Pos t = new Pos(x, y);
                    allSquares.Add(t);
                }

            }
            //System.Windows.Forms.MessageBox.Show("in area: b");

            int a;
            while(allSquares.Count() > 0)
            {
                a = RandomNumber.Between(0, allSquares.Count() - 1);
                randSquares.Add(allSquares[a]);
                allSquares.RemoveAt(a);
            }

            foreach(Pos p in randSquares)
            {
                if(objectIds[p.getX(), p.getY()] == 0)
                {
                    objectIds[p.getX(), p.getY()] = lifeIn.GetObjectId();
                    squareColors[p.getX(), p.getY()] = lifeIn.GetObjectColor();
                    lifeIn.setIsInAnArea();
                    loopControl = 1;
                    break;
                }
            }

            //System.Windows.Forms.MessageBox.Show("in area: c");

            return loopControl;
        }

        internal void recalculateGridRemove(Life lifeIn)
        {
            //System.Windows.Forms.MessageBox.Show(Convert.ToString("i Area: id: " + lifeIn.GetObjectId()));

            Pos temp = ValueClass.getAreaSize();
            int loopControl = 0;
            for(int x = 0; x < temp.getX(); x++)
            {
                for(int y = 0; y < temp.getY(); y++ )
                {
                    if(lifeIn.GetObjectId() == objectIds[x, y])
                    { 
                        objectIds[x, y] = 0;
                        squareColors[x, y] = 0;
                        loopControl = 1;
                    }
                    if (loopControl == 1)
                        {break;}
                }
                if (loopControl == 1)
                { break; }
            }
            if(loopControl == 0)
            {
                // för felsökning - om programmet kommer hit så är det något skumt på gång.
                System.Windows.Forms.MessageBox.Show("i Area: Det gick inte att ta bort objektet" + lifeIn.GetRace() + " id=" + lifeIn.GetObjectId());
            }
        }

        public int[,] getArea()
        {
            return squareColors;
        }

        public int getX()
        { return x; }

        public int getY()
        { return y; }

        internal List<Life> getObjectsInArea()
        { return objectsInThisArea; }
    }
}
