WRC1.1.3.1
Senaste versionen. Jag försöker optimera koden för att få det att gå snabbare.
----------------------------------------------------------------------------------------------------------------------------

WRC1.1.3
Har ändrat update-loopen så alla objekt ligger i en Queue i stället för List. Det verkar faktiskt gå snabbare.

----------------------------------------------------------------------------------------------------------------------------

WRC1.1.2
Denna branch innehåller en update där en räknare visar antalet skapade och existerande objekt, samt antalet iterationer.

----------------------------------------------------------------------------------------------------------------------------
WRC Master branch

Syftet med denna är egentligen flera.

1 - Att lära mig att programmera c#
2 - Att försöka göra en modell över hur populationer förändras över tiden beroende på diverse parametrar
3 - Att försöka göra en slags simulerad evolutionsmodell

När man startar programmet behövs en fet skärm med hög upplösning. Min är gjort på 1680 * 1050, och det är egentligen för få pixlar.

"Världen" består av fyrkanter. World Size X och Y
Dessa fyrkanter innehåller ett antal objekt. Maxantalet definieras av Area Size X och Area Size Y.
Storleken på objekten (som är kvadrater) är Object Squares.
Summan av de tre Density per area får alltså inte vara större än Area Size X * Area Size Y.

Sedan ställer man in de andra parametrarna och försöker få balans på populationerna.
Defaultinställningarna funkar ganska bra.

Jag har kört ett halvt dygn utan buggar eller problem. Antalet objekt som hade skapats och dött var dö större än 11.000.000

Nu måste jag optimera koden så den blir snabbare så jag kan köra med mindre objekt-kvadrater och större yta.

mvh

Magnus