﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zimulator;
//using System.Drawing.Graphics;

namespace WRC01
{
    public partial class Form1 : Form
    {
        internal Zimulator.World omni;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            omni = new World();
            //Form1.Size = new Size(1500, 1000);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // create the "world"
            
            // show how many animals have been created
            //MessageBox.Show(omni.FiatLux());
            omni.FiatLux();
            // start with drawing the squares where the animals will "live"
            InitialPainter();

            // this is the handler that paints the square animals each time an object is added or removed in the world.
            omni._worldArea.AreaHandler += RedrawPosition;

            // adds the initial objects to the areas
            omni.AddObjectsToAreas();
        }


        private void InitialPainter()
        {
            // Paints a coordinate system to make it easier to see which position the animal is in.
            var worldPos = omni.ValueClass.getWorldSize();
            var areaPos = omni.ValueClass.getAreaSize();
            var squarePos = omni.ValueClass.getObjectSquareSize();
            int xTotalSquareSize = areaPos.getX() * squarePos;
            int yTotalSquareSize = areaPos.getY() * squarePos;
            MessageBox.Show("totalsquaresize: " + Convert.ToString(xTotalSquareSize));
            var graphicsObj = CreateGraphics();
            int lT = 1;
            //var myPen = new Pen(Color.Black, lT);
            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);

            //for (int i = 0; i < worldPos.getX(); i++)
            //{
            //    for (int j = 0; j < worldPos.getY(); j++)
            //    {
            //        graphicsObj.FillRectangle(myBrush, new Rectangle(200 + i * (xTotalSquareSize + 2), 10 + j * (yTotalSquareSize + 2), xTotalSquareSize, yTotalSquareSize));
            //        //graphicsObj.DrawLine(myPen, i * areaPos.getX(), 0, i * areaPos.getX(), tempPos.getY() * areaPos.getY());
            //        //graphicsObj.DrawLine(myPen, 0, i * areaPos.getX(), tempPos.getY() * areaPos.getY(), i * areaPos.getX());
            //    }
            //}

        }

        public void RedrawPosition(object sender, Zimulator.Area inArea)                      //ROGERKOD
        {
            // int 0 = tom
            // int 1 = wolf
            // int 2 = rabbit
            // int 3 = carrot

            var worldPos = omni.ValueClass.getWorldSize();
            var areaPos = omni.ValueClass.getAreaSize();
            var squarePos = omni.ValueClass.getObjectSquareSize();
            int xTotalSquareSize = areaPos.getX() * squarePos;
            int yTotalSquareSize = areaPos.getY() * squarePos;

            //int _startingPosX = 200 + (inArea.getX() * (xTotalSquareSize + 2) + 1);
            //int _startingPosY = 10 + inArea.getY() * (yTotalSquareSize + 2) + 1;
            int _startingPosX = 200 + (inArea.getX() * (xTotalSquareSize) + 1);
            int _startingPosY = 10 + inArea.getY() * (yTotalSquareSize) + 1;
            int x;
            int y;

            var graphicsObj = CreateGraphics();
            System.Drawing.SolidBrush myBrush0 = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            System.Drawing.SolidBrush myBrush1 = new System.Drawing.SolidBrush(System.Drawing.Color.Gray);
            System.Drawing.SolidBrush myBrush2 = new System.Drawing.SolidBrush(System.Drawing.Color.White);
            System.Drawing.SolidBrush myBrush3 = new System.Drawing.SolidBrush(System.Drawing.Color.Orange);

            int[,] objectArea = inArea.getArea();
            for(int i = 0; i <= objectArea.GetUpperBound(0); i++)
            {
                for(int j = 0; j <= objectArea.GetUpperBound(1); j++)
                {
                    if (objectArea[i, j] == 0)
                    {
                        x = _startingPosX + (i * squarePos);
                        y = _startingPosY + (j * squarePos);
                        graphicsObj.FillRectangle(myBrush0, new Rectangle(x, y, squarePos, squarePos));
                        int abc = 0;
                    }
                    
                    if(objectArea[i, j] == 1)
                    {
                        x = _startingPosX + (i * squarePos);
                        y = _startingPosY + (j * squarePos);
                        graphicsObj.FillRectangle(myBrush1, new Rectangle(x, y, squarePos, squarePos));
                    }

                    if(objectArea[i, j] == 2)
                    {
                        x = _startingPosX + (i * squarePos);
                        y = _startingPosY + (j * squarePos);
                        graphicsObj.FillRectangle(myBrush2, new Rectangle(x, y, squarePos, squarePos));
                    }

                    if(objectArea[i, j] == 3)
                    {
                        x = _startingPosX + (i * squarePos);
                        y = _startingPosY + (j * squarePos);
                        graphicsObj.FillRectangle(myBrush3, new Rectangle(x, y, squarePos, squarePos));
                        //MessageBox.Show("carrot coming up!");
                    }


                }
            }
            
        }


        //private void AddObject(Life life)
        //{
        //    //Invoke on main thread if needed.
        //    if (InvokeRequired)
        //    {
        //        Invoke(new Action<Life>(AddObject), life);
        //    }
        //    else
        //    {
        //        // Just show position to start with - we have to remove the old position later
        //        Graphics graphicsObj = CreateGraphics();
        //        Pen myPen = new Pen(Color.Red, 2);
        //        Rectangle myRectangle = new Rectangle(life.getPosX(), life.getPosY(), 4, 4);
        //        graphicsObj.DrawRectangle(myPen, myRectangle);
        //    }
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            omni.StopTheWorld();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            World a = new World();
            a.FiatLux();
            Pos b = new Pos(2, 4);
            //Zimulator.NewRandomClosePos rcl = new Zimulator.NewRandomClosePos();
            NewRandomClosePos nrcp = new NewRandomClosePos();
            Pos c = nrcp.getNewPos(b, 2, false, ref omni.ValueClass);
            MessageBox.Show("i Forms1: x: " + c.getX() + " and y:" + c.getY());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" i Forms1: " + Convert.ToString(Zimulator.RandomNumber.Between(-2, 2)));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            omni.RunTheWorld();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        

    }


}
